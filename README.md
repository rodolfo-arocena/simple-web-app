simple-web-app
==============

Simple web application that demonstrates the use of the OpenID Connect client code and configuration

## Setup

### Configure properties

You will also need to modify the following file:

-  `src/main/webapp/WEB-INF/servlet.properties`
  - `server.url=https://your server url` Replace "https://your server url" with your server url

- src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml`
  - Change all "https://localhost:9443..." with tour server url

### KeyStore

To add https to the webapp ins necessary indicate the path to the keystore in`etc/jetty/jetty-ss.xml`, in this file modify value of **KeyStorePath** and  **KeyStorePassword**.

To call a open id connect server  with ssl invalid certificate, modify `javax.net.ssl.trustStore` and `javax.net.ssl.trustStorePassword`

### Run Jetty
From the root directory, run the following command:

```bash
mvn clean package jetty:run-forked
```
